<h1 align="center">
  Hello Padawans
  <img src="https://media.giphy.com/media/nDg8O2z3Rmk6Y/source.gif" width="30px">
</h1>

<p align="right"> <img src="https://komarev.com/ghpvc/?username=zaujulio&color=blue" alt="zaujulio" /> </p>
<img align="right" src="./thinking.svg" width=300>

I'm at university, always researching and learning something new = )

I am currently learning about artificial intelligence, but I am also interested in cloud, web and graphic design. I'll tell you right away, I love C, Python, Typescript and Linux.

- 📧 Talk to me by email, remember to bring a cup of coffee...
- 🐦 Follow me there on Twitter, GitHub updates :D

___

![info](./github-metrics.svg)

<p align="center">
  <a id="twitter" href="https://twitter.com/Zau_Galvao" target="_blank">
    <img src="https://img.shields.io/badge/Twitter-1DA1F2?style=for-the-badge&logo=twitter&logoColor=white" alt="_" />
  </a>

  <a id="linkedin" href="https://www.linkedin.com/in/zaujulio" target="_blank">
    <img src="https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white" alt="_" />
  </a>

  <a id="instagram" href="https://www.instagram.com/ZauJulio/" target="_blank">
    <img src="https://img.shields.io/badge/Instagram-E4405F?style=for-the-badge&logo=instagram&logoColor=white" alt="_" />
  </a>

  <a id="spotify" href="https://open.spotify.com/user/22h43nfzwiryoykpab2bd76ha?si=r7hAIFhvRUqrQylhZaep7g" target="_blank">
    <img src="https://img.shields.io/badge/Spotify-1ED760?&style=for-the-badge&logo=spotify&logoColor=white" alt="_" />
  </a>

  <a id="whatsapp" href="https://api.whatsapp.com/send?phone=5584998651868&text=Hello" target="_blank">
    <img src="https://img.shields.io/badge/WhatsApp-25D366?style=for-the-badge&logo=whatsapp&logoColor=white" alt="_" />
  </a>
</p>

<p id="badgets" align="center">
  <a id="telegram" href="https://t.me/ZauJulio" target="_blank">
    <img src="https://img.shields.io/badge/Telegram-2CA5E0?style=for-the-badge&logo=telegram&logoColor=white" alt="_" />
  </a>

  <a id="gmail" href="http://zauhdf@gmail.com/" target="_blank">
    <img src="https://img.shields.io/badge/Gmail-D14836?style=for-the-badge&logo=gmail&logoColor=white" alt="_" />
  </a>

  <a id="codersrank" href="https://profile.codersrank.io/user/zaujulio" target="_blank">
    <img width="114" height="28" src="https://i.imgur.com/EQnULDd.png" alt="_" />
  </a>

  <a id="figma" href="https://www.figma.com/@zaujulio" target="_blank">
    <img src="https://img.shields.io/badge/Figma-F24E1E?style=for-the-badge&logo=figma&logoColor=white" alt="_" />
  </a>

  <a id="stackoverflow" href="https://stackoverflow.com/users/11448162/zaujulio" target="_blank">
    <img src="https://img.shields.io/badge/Stack_Overflow-FE7A16?style=for-the-badge&logo=stack-overflow&logoColor=white" alt="_" />
  </a>

  <a id="deviantart" href="https://www.deviantart.com/zaujulio" target="_blank">
    <img src="https://img.shields.io/badge/DeviantArt-05CC47?style=for-the-badge&logo=deviantart&logoColor=white" alt="_" />
  </a>
</p>
